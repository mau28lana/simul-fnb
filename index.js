import express from "express";
import db from "./config/Database.js";
import router from "./routes/index.js";
import dotenv from "dotenv";
import cors from "cors";
import cookieParser from "cookie-parser";

dotenv.config();
const app = express();


try{
    await db.authenticate();
    console.log("Database Connected...");
}catch(e){
    console.error(e);
}

app.use(cors({credentials: true,origin:'http://localhost:3000'}));
app.use(cookieParser());
app.use(express.json());
app.use(router);

app.listen(5000, ()=> console.log("Server running at port 5000"));